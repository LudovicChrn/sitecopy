window.onscroll = function() {myFunction()};

var header = document.getElementById("header");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}

// Gif Jeux
function hoverBuy(element) {
  element.setAttribute("src", "./picture/menu-buy.gif");
}

function unhoverBuy(element) {
  element.setAttribute("src", "./picture/menu-buy-reversed.gif");
}

// Gif Communauté
function hoverRealms(element) {
  element.setAttribute("src", "./picture/menu-realms.gif");
}

function unhoverRealms(element) {
  element.setAttribute("src", "./picture/menu-realms-reversed.gif");
}

// Gif Produits Dérivés
function hoverStore(element) {
  element.setAttribute("src", "./picture/menu-store.gif");
}

function unhoverStore(element) {
  element.setAttribute("src", "./picture/menu-store-reversed.gif");
}

// Gif Support
function hoverSupport(element) {
  element.setAttribute("src", "./picture/menu-support.gif");
}

function unhoverSupport(element) {
  element.setAttribute("src", "./picture/menu-support-reversed.gif");
}

function myFunction(x) {
  x.classList.toggle("change");
}

function changeMenu(element){
  if (element.clicked === true){
      element.setAttribute("src", "./picture/menu-hamburger--close.gif");
  } else {
    element.setAttribute("src", "./picture/menu-hamburger--open.gif");
  }

}